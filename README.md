# **BRAND PIT**

### **Introduction**
This project is an application of the Django tutorials.

## **FEATURES**

### **Polling**
With Brand Pit, you can create voting polls based on categories and brands.

### **Discussion**
When talking about brands, everybody loves a nice ol' debate.
You can write what you like about a specific brand and what you don't like about it.

### **Authentication**
Sign up for a free account, then start voting for your favorite brands!

## **lancegab.pythonanywhere.com**
