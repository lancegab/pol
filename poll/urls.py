from django.conf.urls import url
from . import views

app_name = 'poll'

urlpatterns = [

    # /poll/
    url(r'^$', views.IndexView.as_view(), name='index'),

    # /poll/5/
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),

    # /poll/desc-6/
    url(r'^choice-(?P<pk>[0-9]+)/$',
        views.DescriptionView.as_view(), name='description'),

    # /poll/5/vote/
    url(r'^(?P<topic_id>[0-9]+)/vote/$',
        views.VoteView.as_view(), name='vote'),


    # /poll/desc-6/
    url(r'^d-(?P<choice_id>[0-9]+)/comment$',
        views.AddCommentView.as_view(), name='comment'),

    # /poll/add/
    url(r'^add/$', views.AddView.as_view(), name='add'),

]
