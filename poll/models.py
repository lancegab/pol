# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


class Topic(models.Model):
    topic_text = models.CharField(max_length=150)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    pub_date = models.DateTimeField('date added')

    def __str__(self):
        return self.topic_text


class Choice(models.Model):
    choice_text = models.CharField(max_length=150)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    votes = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    pub_date = models.DateTimeField('date added')

    def __str__(self):
        return self.choice_text


class Comment(models.Model):
    comment_text = models.CharField(max_length=1000)
    type = models.IntegerField(default=0)
    choice = models.ForeignKey(Choice, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    pub_date = models.DateTimeField('date added')

    def __str__(self):
        return self.comment_text


class Vote(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)

    def __str__(self):
        return self.topic + ' - ' + self.user
