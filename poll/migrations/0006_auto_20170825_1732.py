# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-25 17:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('poll', '0005_comment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='type',
            field=models.IntegerField(default=0),
        ),
    ]
