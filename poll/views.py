# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.core.urlresolvers import reverse
from django.views import generic
from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Topic, Choice, Comment, Vote


class IndexView(LoginRequiredMixin, generic.ListView):
    """View for showing choices all polls"""
    login_url = 'registration:sign_in'
    redirect_field_name = 'redirect_to'

    template_name = 'poll/index.html'
    context_object_name = 'topic_list'

    def get_queryset(self):
        """Returns all polls, ordered by date"""
        return Topic.objects.order_by('-pub_date')


class DetailView(LoginRequiredMixin, generic.DetailView):
    """View for showing choices in a respective poll"""
    login_url = 'registration:sign_in'
    redirect_field_name = 'redirect_to'

    model = Topic
    template_name = 'poll/detail.html'


class DescriptionView(LoginRequiredMixin, generic.DetailView):
    """View for poll choice description (which shows comments)"""
    login_url = 'registration:sign_in'
    redirect_field_name = 'redirect_to'

    model = Choice
    template_name = 'poll/description.html'

    def get_context_data(self, **kwargs):
        """Returns pro and con comments"""

        context = super(DescriptionView, self).get_context_data(**kwargs)
        pro = Comment.objects.filter(type=1, choice=self.kwargs['pk'])
        con = Comment.objects.filter(type=-1, choice=self.kwargs['pk'])

        context['pro_comments'] = pro
        context['con_comments'] = con
        return context


class AddView(LoginRequiredMixin, generic.View):
    """View for adding new poll"""
    login_url = 'registration:sign_in'
    redirect_field_name = 'redirect_to'
    template_name = 'poll/add_poll.html'

    def get(self, request):
        """Returns a form to add new poll"""
        context = {}
        return render(request, self.template_name, context=context)

    def post(self, request):
        """Saves new poll data to database, then redirects to index page"""
        new_topic = Topic(topic_text=request.POST['topic_text'],
                          user=request.user, pub_date=timezone.now())
        new_topic.save()

        i = 1
        while('choice_' + str(i) in request.POST):
            choice = 'choice_' + str(i)
            if request.POST[choice] != '':

                new_choice = Choice(choice_text=request.POST[choice],
                                    topic=new_topic, user=request.user,
                                    pub_date=timezone.now())
                new_choice.save()
            i += 1

        return HttpResponseRedirect(reverse('poll:index'))


class VoteView(LoginRequiredMixin, generic.View):
    """View for voting"""
    login_url = 'registration:sign_in'
    redirect_field_name = 'redirect_to'

    def post(self, request, topic_id):
        """Increments vote based on user's choice. Otherwise, returns error"""

        topic = get_object_or_404(Topic, pk=topic_id)

        poll_vote = Vote.objects.filter(user=request.user, topic=topic)

        if poll_vote.exists():
            return render(request, 'poll/detail.html', {
                'topic': topic,
                'error_message': "You've already voted.",
            })

        try:
            selected_choice = topic.choice_set.get(id=request.POST['choice'])
        except (KeyError, Choice.DoesNotExist):
            return render(request, 'poll/detail.html', {
                'topic': topic,
                'error_message': "You didn't select a choice.",
            })
        else:
            selected_choice.votes += 1
            selected_choice.save()
            new_vote = Vote(user=request.user, topic=topic)
            new_vote.save()

            return HttpResponseRedirect(reverse('poll:detail',
                                        args=(topic.id,)))


class AddCommentView(LoginRequiredMixin, generic.View):
    """View for adding comments"""
    login_url = 'registration:sign_in'
    redirect_field_name = 'redirect_to'

    def post(self, request, choice_id):
        """When successful, saves comment to database and return success status.
           Otherwise, return failed status"""
        choice = get_object_or_404(Choice, pk=choice_id)
        # if request.POST['comment_text'] == '':
        #     return JsonResponse({'status': 'failed'})
        new_comment = Comment(comment_text=request.POST['comment_text'],
                              type=request.POST['type'], choice=choice,
                              user=request.user, pub_date=timezone.now())
        new_comment.save()

        return JsonResponse({'status': 'success',
                             'comment_text': new_comment.comment_text,
                             'comment_user': new_comment.user.username,
                             'pub_date': new_comment.pub_date})
