from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse

from django.contrib.auth.mixins import LoginRequiredMixin


class HomepageView(LoginRequiredMixin, TemplateView):
    login_url = 'registration:sign_in'
    redirect_field_name = 'redirect_to'

    def get(self, request):
        return HttpResponseRedirect(reverse('poll:index'))
