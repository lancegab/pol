require([
    'jquery',
    'mustache.min'
],function($,Mustache){

    $('#add_comment_pro').on('submit',function(e){
        e.preventDefault();
        var comment_template =  "<div class='card-comment-pro'>"
                                +"<div class='card-comment-content'>"
                                +    "<p>{{ comment_text }}</p>"
                                +"</div>"
                                +"<div class='card-comment-user'>"
                                +    "<p>{{ comment_user }}</p>"
                                +"</div>"
                                +"<div class='card-comment-date'>"
                                +    "<p>{{ pub_date }}</p>"
                                +"</div>"
                                +"</div>"
        $.ajax({
            type: 'POST',
            url: $('#add_comment_pro').attr('action'),
            data:{
                comment_text: $('#comment_text_pro').val(),
                type: 1,
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
            },
            success:function(data){
                if(data.status=='failed'){
                    alert('Failed')
                }
                else{
                    $('#comment_text_pro').val('');
                    $('#pro_comments').append(Mustache.render(comment_template,data));
                }
            }
        });
    });
    $('#add_comment_con').on('submit',function(e){
        e.preventDefault();
        var comment_template =  "<div class='card-comment-con'>"
                                +"<div class='card-comment-content'>"
                                +    "<p>{{ comment_text }}</p>"
                                +"</div>"
                                +"<div class='card-comment-user'>"
                                +    "<p>{{ comment_user }}</p>"
                                +"</div>"
                                +"<div class='card-comment-date'>"
                                +    "<p>{{ pub_date }}</p>"
                                +"</div>"
                                +"</div>"
        $.ajax({
            type: 'POST',
            url: $('#add_comment_con').attr('action'),
            data:{
                comment_text: $('#comment_text_con').val(),
                type: -1,
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
            },
            success:function(data){
                if(data.status=='failed'){
                    alert('Failed');
                }
                else{
                    $('#comment_text_con').val('');
                    $('#con_comments').append(Mustache.render(comment_template,data));
                }
            }
        });
    });
});

var i = 3;
function appendChoiceField() {
    var field = "<div class='form-group'>"
                + "<input type='text' name='choice_" + i
                + "' placeholder='Choice " + i
                + "' class='form-control' autocomplete='off' id='choice'>"
                + "</div>";
    i++;
    $(".choice_container").append(field);
}

function removeChoiceField(){
    if(i > 3){
        i--;
        $('.choice_container').children().last().remove();
    }
}
