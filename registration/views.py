# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.views.generic import View
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.views import generic

from .forms import UserForm


# Create your views here.
class SignUpView(View):
    """View for sign up"""
    form_class = UserForm
    template_name = 'registration/sign_up.html'

    def get(self, request):
        """Renders template that displays a sign up form"""
        form = UserForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        """Creates account and redirects to index on success"""
        form = UserForm(self.request.POST)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('poll:index'))
        else:
            return HttpResponseRedirect(reverse('registration:sign_up'))


class SignInView(View):
    """View for sign in"""
    template_name = 'registration/sign_in.html'

    def get(self, request):
        """Renders template for signing in"""
        context = {}
        return render(request, self.template_name, context=context)

    def post(self, request):
        """Redirects to index on successful authentication
           Otherwise, renders login page."""
        context = {}
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('poll:index'))
        else:
            context['error'] = 'Invalid Username of Password'
            context['username'] = username

        return render(request, 'registration/sign_in.html', context=context)


class SignOutView(View):
    """View for sign out"""
    def get(self, request):
        """Logout user's session then redirects to login page"""
        logout(request)
        return HttpResponseRedirect(reverse('registration:sign_in'))
