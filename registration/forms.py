from django import forms
from django.contrib.auth.models import User


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    def save(self, commit=True):
        user = super(UserForm, self).save(commit=False)
        password = self.cleaned_data['password']
        user.set_password(password)
        user.save()

    class Meta:
        model = User
        fields = ['username', 'email', 'password']
