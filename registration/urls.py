from django.conf.urls import url
# from django.contrib.auth import views as auth_views

from . import views

app_name = 'registration'

urlpatterns = [
    url(r'^sign-in/$', views.SignInView.as_view(), name='sign_in'),
    url(r'^sign-up/$', views.SignUpView.as_view(), name='sign_up'),
    url(r'^sign-out/$', views.SignOutView.as_view(), name='sign_out'),
]
